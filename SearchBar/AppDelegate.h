//
//  AppDelegate.h
//  SearchBar
//
//  Created by SI3 on 2018/05/28.
//  Copyright © 2018年 D.I.System co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

