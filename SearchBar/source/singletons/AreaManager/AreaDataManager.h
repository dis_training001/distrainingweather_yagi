//
//  AreaDataManager.h
//  SearchBar
//
//  Created by SI3 on 2018/06/04.
//  Copyright © 2018年 D.I.System co.,ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AreaDataManagerDelegate <NSObject>

@end

@interface AreaDataManager : NSObject{
@public NSDictionary *japan ;
}
+ (AreaDataManager *)ManageData;

@property (weak, nonatomic) id<AreaDataManagerDelegate> delegate;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSDictionary *getDictionaryData ;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSArray *getAreaData;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSArray *jsonAddressArray;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSDictionary *getDictionaryKeyData;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSDictionary *getWeatherData;
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSDictionary *getAreaNumber;
@end

