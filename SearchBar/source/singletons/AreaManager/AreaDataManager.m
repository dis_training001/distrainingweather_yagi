//
//  AreaDataManager.m
//  SearchBar
//
//  Created by SI3 on 2018/06/04.
//  Copyright © 2018年 D.I.System co.,ltd. All rights reserved.
//


#import "AreaDataManager.h"

@interface AreaDataManager ()
@end

@implementation AreaDataManager

static AreaDataManager *ManageData = nil;

+ (AreaDataManager *)ManageData{
    if (!ManageData) {
        ManageData = [AreaDataManager new];
    }
    return ManageData;
}

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

-(NSArray*)jsonAddressArray{
    NSArray *address = @[@"http://api.openweathermap.org/data/2.5/group?id=2130629,2130656,2112518,2111888,2113124,2110554,2112922,2112669&appid=216d8cade8f1f3ea1eb8d03d017f660d",
                         
                         @"http://api.openweathermap.org/data/2.5/group?id=1850310,1863501,1853226,2113014,1850147,1860291,1855429,1849872,1861387,1863983,1848649,1856210,1863640,1851715,1865694,1853908,1855608,1857352,1857910&appid=216d8cade8f1f3ea1eb8d03d017f660d",
                         
                         @"http://api.openweathermap.org/data/2.5/group?id=1847966,1848938,1852553,1849890,1852442,1854381,1862413,1848681,1850157,1860834,1864226,1856392,1863958,1853299,1856156,1858419,1864750,1856710,7303136,1894616&appid=216d8cade8f1f3ea1eb8d03d017f660d"];

    return address;
}



-(NSDictionary*)getDictionaryData{
    
    NSDictionary *japan = @{@"Asahikawa":@"北海道",@"Aomori-ken":@"青森県",@"Iwate-ken":@"岩手県",
                            @"Miyagi-ken":@"宮城県",@"Akita-ken":@"秋田県",@"Yamagata-ken":@"山形県",
                            @"Fukushima-ken":@"福島県",@"Ibaraki-ken":@"茨城県",@"Gunma-ken":@"群馬県",
                            @"Tochigi-ken":@"栃木県",@"Saitama-ken":@"埼玉県",@"Chiba-ken":@"千葉県",
                            @"Tokyo":@"東京",@"Kanagawa-ken":@"神奈川県",@"Niigata-ken":@"新潟県",
                            @"Toyama-ken":@"富山県",@"Ishikawa-ken":@"石川県",@"Fukui-ken":@"福井県",
                            @"Yamanashi-ken":@"山梨県",@"Nagano-ken":@"長野県",@"Gifu-ken":@"岐阜県",
                            @"Shizuoka-ken":@"静岡県",@"Aichi-ken":@"愛知県",@"Osaka":@"大阪",
                            @"Nara-ken":@"奈良県",@"Mie-ken":@"三重県",@"Kyoto":@"京都",
                            @"Akashi":@"兵庫県",@"Wakayama-ken":@"和歌山県",@"Shiga-ken":@"滋賀県",
                            @"Tottori-ken":@"鳥取県",@"Shimane-ken":@"島根県",@"Okayama-ken":@"岡山県",
                            @"Hiroshima-ken":@"広島県", @"Yamaguchi-ken":@"山口県", @"Tokushima-ken":@"徳島県",
                            @"Kagawa-ken":@"香川県",@"Ehime-ken":@"愛媛県", @"Muroto":@"高知県",
                            @"Fukuoka-ken":@"福岡県",@"Saga-ken":@"佐賀県",@"Nagasaki-ken":@"長崎県",
                            @"Kumamoto-ken":@"熊本県", @"Beppu":@"大分県",@"Miyazaki-ken":@"宮崎県",
                            @"kagoshima":@"鹿児島県",@"Okinawa":@"沖縄県"};
    
    return japan;
}


-(NSDictionary*)getAreaNumber{
    
    NSDictionary *areaNumber = @{@"Asahikawa":@"2130629",@"Aomori-ken":@"2130656",@"Iwate-ken":@"2112518",
                                 @"Miyagi-ken":@"2111888",@"Akita-ken":@"2113124",@"Yamagata-ken":@"2110554",
                                 @"Fukushima-ken":@"2112922",@"Ibaraki-ken":@"2112669",@"Gunma-ken":@"1863501",
                                 @"Tochigi-ken":@"1850310",@"Saitama-ken":@"1853226",@"Chiba-ken":@"2113014",
                                 @"Tokyo":@"1850147",@"Kanagawa-ken":@"1860291",@"Niigata-ken":@"1855429",
                                 @"Toyama-ken":@"1849872",@"Ishikawa-ken":@"1861387",@"Fukui-ken":@"1863983",
                                 @"Yamanashi-ken":@"1848649",@"Nagano-ken":@"1856210",@"Gifu-ken":@"1863640",
                                 @"Shizuoka-ken":@"1851715",@"Aichi-ken":@"1865694",@"Osaka":@"1853908",
                                 @"Nara-ken":@"1855608",@"Mie-ken":@"1857352",@"Kyoto":@"1857910",
                                 @"Akashi":@"1847966",@"Wakayama-ken":@"1848938",@"Shiga-ken":@"1852553",
                                 @"Tottori-ken":@"1849890",@"Shimane-ken":@"1852442",@"Okayama-ken":@"1854381",
                                 @"Hiroshima-ken":@"1862413", @"Yamaguchi-ken":@"1848681", @"Tokushima-ken":@"1850157",
                                 @"Kagawa-ken":@"1860834",@"Ehime-ken":@"1864226", @"Muroto":@"1856392",
                                 @"Fukuoka-ken":@"1863958",@"Saga-ken":@"1853299",@"Nagasaki-ken":@"1856156",
                                 @"Kumamoto-ken":@"1858419", @"Beppu":@"1864750",@"Miyazaki-ken":@"1856710",
                                 @"kagoshima":@"7303136",@"Okinawa":@"1894616"};
    
    return areaNumber;
}


-(NSArray*)getAreaData{
    
    NSArray *japanAreaName = @[@"北海道", @"青森県", @"岩手県", @"宮城県", @"秋田県", @"山形県",
                               @"福島県", @"茨城県", @"群馬県", @"栃木県", @"埼玉県", @"千葉県",
                               @"東京", @"神奈川県", @"新潟県", @"富山県", @"石川県", @"福井県",
                               @"山梨県", @"長野県", @"岐阜県", @"静岡県", @"愛知県", @"大阪",
                               @"奈良県", @"三重県", @"京都", @"兵庫県", @"和歌山県", @"滋賀県",
                               @"鳥取県", @"島根県", @"岡山県", @"広島県", @"山口県", @"徳島県",
                               @"香川県", @"愛媛県", @"高知県", @"福岡県", @"佐賀県", @"長崎県",
                               @"熊本県",@"大分県", @"宮崎県", @"鹿児島県", @"沖縄県",
                               @"ほっかいどう", @"あおもりけん", @"いわてけん", @"みやぎけん", @"あきたけん", @"やまがたけん",
                               @"ふくしまけん", @"いばらきけん", @"ぐんまけん", @"とちぎけん", @"さいたまけん", @"ちばけん",
                               @"とうきょう", @"かながわけん", @"にいがたけん", @"とやまけん", @"いしかわけん", @"ふくいけん",
                               @"やまなしけん", @"ながのけん", @"ぎふけん", @"しずおかけん", @"あいちけん", @"おおさか",
                               @"ならけん", @"みえけん", @"きょうと", @"ひょうごけん", @"わかやまけん", @"しがけん",
                               @"とっとりけん", @"しまねけん", @"おかやまけん", @"ひろしまけん", @"やまぐちけん", @"とくしまけん",
                               @"かがわけん", @"えひめけん", @"こうちけん", @"ふくおかけん", @"さがけん", @"ながさきけん",
                               @"くまもとけん",@"おおいたけん", @"みやざきけん", @"かごしまけん", @"おきなわけん"];
    
    return japanAreaName ;
}


-(NSDictionary*)getDictionaryKeyData{
    
    NSDictionary *reverseJapan = @{@"北海道":@"Asahikawa",@"青森県":@"Aomori-ken",@"岩手県":@"Iwate-ken",
                                   @"宮城県":@"Miyagi-ken",@"秋田県":@"Akita-ken",@"山形県":@"Yamagata-ken",
                                   @"福島県":@"Fukushima-ken",@"茨城県":@"Ibaraki-ken",@"群馬県":@"Gunma-ken",
                                   @"栃木県":@"Tochigi-ken",@"埼玉県":@"Saitama-ken",@"千葉県":@"Chiba-ken",
                                   @"東京":@"Tokyo",@"神奈川県":@"Kanagawa-ken",@"新潟県":@"Niigata-ken",
                                   @"富山県":@"Toyama-ken",@"石川県":@"Ishikawa-ken",@"福井県":@"Fukui-ken",
                                   @"山梨県":@"Yamanashi-ken", @"長野県":@"Nagano-ken",@"岐阜県":@"Gifu-ken",
                                   @"静岡県":@"Shizuoka-ken",@"愛知県":@"Aichi-ken",@"大阪":@"Osaka",
                                   @"奈良県":@"Nara-ken",@"三重県":@"Mie-ken",@"京都":@"Kyoto",@"兵庫県":@"Akashi", @"和歌山県":@"Wakayama-ken",@"滋賀県":@"Shiga-ken",
                                   @"鳥取県":@"Tottori-ken",@"島根県":@"Shimane-ken",@"岡山県":@"Okayama-ken",
                                   @"広島県":@"Hiroshima-ken",@"山口県":@"Yamaguchi-ken",@"徳島県":@"Tokushima-ken",
                                   @"香川県":@"Kagawa-ken",@"愛媛県":@"Ehime-ken",@"高知県":@"Muroto",@"福岡県":@"Fukuoka-ken", @"佐賀県":@"Saga-ken",
                                   @"長崎県":@"Nagasaki-ken",@"熊本県":@"Kumamoto-ken",@"大分県":@"Beppu",@"Miyazaki-ken":@"宮崎県",
                                   @"鹿児島県":@"kagoshima",@"沖縄県":@"Okinawa",
                                   @"ほっかいどう":@"Asahikawa",@"あおもりけん":@"Aomori-ken",@"いわてけん":@"Iwate-ken",
                                   @"みやぎけん":@"Miyagi-ken",@"あきたけん":@"Akita-ken",@"やまがたけん":@"Yamagata-ken",
                                   @"ふくしまけん":@"Fukushima-ken",@"いばらきけん":@"Ibaraki-ken",@"ぐんまけん":@"Gunma-ken",
                                   @"とちぎけん":@"Tochigi-ken",@"さいたまけん":@"Saitama-ken",@"ちばけん":@"Chiba-ken",
                                   @"とうきょう":@"Tokyo",@"かながわけん":@"Kanagawa-ken",@"にいがたけん":@"Niigata-ken",
                                   @"とやまけん":@"Toyama-ken",@"いしかわけん":@"Ishikawa-ken",@"ふくいけん":@"Fukui-ken",
                                   @"やまなしけん":@"Yamanashi-ken", @"ながのけん":@"Nagano-ken",@"ぎふけん":@"Gifu-ken",
                                   @"しずおかけん":@"Shizuoka-ken",@"あいちけん":@"Aichi-ken",@"おおさか":@"Osaka",
                                   @"ならけん":@"Nara-ken",@"みえけん":@"Mie-ken",@"きょうと":@"Kyoto",@"ひょうごけん":@"Akashi",
                                   @"わかやまけん":@"Wakayama-ken",@"しがけん":@"Shiga-ken",
                                   @"とっとりけん":@"Tottori-ken",@"しまねけん":@"Shimane-ken",@"おかやまけん":@"Okayama-ken",
                                   @"ひろしまけん":@"Hiroshima-ken",@"やまぐちけん":@"Yamaguchi-ken",@"とくしまけん":@"Tokushima-ken",
                                   @"かがわけん":@"Kagawa-ken",@"えひめけん":@"Ehime-ken",@"こうちけん":@"Muroto",@"ふくおかけん":@"Fukuoka-ken", @"さがけん":@"Saga-ken",
                                   @"ながさきけん":@"Nagasaki-ken",@"くまもとけん":@"Kumamoto-ken",@"おおいたけん":@"Beppu",@"みやざきけん":@"Miyazaki-ken",
                                   @"かごしまけん":@"kagoshima",@"おきなわけん":@"Okinawa"
                                    };
    
    return reverseJapan ;
}

-(NSDictionary*)getWeatherData{
    
    NSDictionary *weather = @{@"Clear":@"晴れ",@"Ash":@"灰",@"Clouds":@"曇り",@"Rain":@"雨",@"Haze":@"煙霧",
                              @"Mist":@"霧",@"Fog":@"濃霧",@"Snow":@"雪",@"Thunderstorm":@"雷雨",@"Drizzle":@"霧雨"
                              };
    
    return weather ;
}
@end
