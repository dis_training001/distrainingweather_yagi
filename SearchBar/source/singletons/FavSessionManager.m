//
//  FavSessionManager.m
//  SearchBar
//
//  Created by SI3 on 2018/06/08.
//  Copyright © 2018年 D.I.System co.,ltd. All rights reserved.
//

#import "FavSessionManager.h"

@interface FavSessionManager () <NSURLSessionDelegate>
@end

@implementation FavSessionManager

static FavSessionManager *sharedData = nil;
NSMutableData *favResponseData;


+ (FavSessionManager *)sharedManager{
    @synchronized(self){
        if (!sharedData) {
            sharedData = [FavSessionManager new];
        }
    }
    return sharedData;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        self.session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
    }
    return self;
}


-(void)getWeatherData:(NSString *)getUrl{

    NSString* urlString = getUrl;
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
    NSURLSessionDataTask *task = [session dataTaskWithURL:url];
    [task resume];

}
- (void)URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
didReceiveResponse:(NSURLResponse *)response
 completionHandler:(void (^)(NSURLSessionResponseDisposition))completionHandler {

    favResponseData = [NSMutableData data];

    completionHandler(NSURLSessionResponseAllow);
}

- (void)URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
    didReceiveData:(NSData *)data {

    [favResponseData appendData:data];
}

- (void)URLSession:(NSURLSession *)session
              task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error {

    NSDictionary *favJson;
    favJson = [NSJSONSerialization JSONObjectWithData:favResponseData options:NSJSONReadingAllowFragments error:nil];

    [self.delegate sendFavJsonData:favJson];

}
@end
