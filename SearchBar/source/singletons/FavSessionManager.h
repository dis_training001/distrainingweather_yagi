//
//  FavFavSessionManager.h
//  SearchBar
//
//  Created by SI3 on 2018/06/08.
//  Copyright © 2018年 D.I.System co.,ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FavSessionManagerDelegate <NSObject>
- (void)sendFavJsonData:(NSDictionary *)Favjson;
@end

@interface FavSessionManager : NSObject{
@public NSDictionary *japan ;
}

@property (strong, nonatomic) NSURLSession *session;
@property (weak, nonatomic) id <FavSessionManagerDelegate> delegate;

+ (FavSessionManager *)sharedManager;
- (void)getWeatherData:(NSString *)getUrl;
@end
