//
//  SessionManager.m
//  TableOtenki
//
//  Created by SI3 on 2018/05/21.
//  Copyright © 2018年 D.I.System co.,ltd. All rights reserved.
//

#import "SessionManager.h"


@interface SessionManager () <NSURLSessionDelegate>
@end

@implementation SessionManager



static SessionManager *sharedData = nil;
NSMutableData *responseData;

+ (SessionManager *)sharedManager{
    @synchronized(self){
        if (!sharedData) {
            sharedData = [SessionManager new];
        }
    }
    return sharedData;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        self.session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
    }
    return self;
}


-(void)getWeatherData:(NSString *)getUrl{
    
    NSString* urlString = getUrl;
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
    NSURLSessionDataTask *task = [session dataTaskWithURL:url];
    [task resume];
    
}
- (void)URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
didReceiveResponse:(NSURLResponse *)response
 completionHandler:(void (^)(NSURLSessionResponseDisposition))completionHandler {
    
    responseData = [NSMutableData data];
    
    completionHandler(NSURLSessionResponseAllow);
}

- (void)URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
    didReceiveData:(NSData *)data {
    
    [responseData appendData:data];
}

- (void)URLSession:(NSURLSession *)session
              task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error {
    
    NSDictionary *json;
    json = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:nil];
    
    [self.delegate sendJsonData:json];
    
}


@end
