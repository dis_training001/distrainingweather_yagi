//
//  SessionManager.h
//  TableOtenki
//
//  Created by SI3 on 2018/05/21.
//  Copyright © 2018年 D.I.System co.,ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol SessionManagerDelegate <NSObject>
- (void)sendJsonData:(NSDictionary *)json;
@end

@interface SessionManager : NSObject{
@public NSDictionary *japan ;
}

@property (strong, nonatomic) NSURLSession *session;
@property (weak, nonatomic) id <SessionManagerDelegate> delegate;

+ (SessionManager *)sharedManager;
- (void)getWeatherData:(NSString *)getUrl;
@end

