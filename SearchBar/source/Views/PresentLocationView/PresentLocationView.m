//
//  PresentLocationView.m
//  SearchBar
//
//  Created by SI3 on 2018/06/05.
//  Copyright © 2018年 D.I.System co.,ltd. All rights reserved.
//

#import "PresentLocationView.h"
#import <CoreLocation/CoreLocation.h>
#import "SessionManager.h"
#import "AreaDataManager.h"
@interface PresentLocationView ()<NSURLSessionDelegate,CLLocationManagerDelegate,AreaDataManagerDelegate>
@property (strong, nonatomic) AreaDataManager *areaDataManager;
@property (weak, nonatomic) IBOutlet UILabel *weatherLabel;
@property (nonatomic,retain) CLLocationManager *locationManager ;
- (IBAction)weatherGetButton:(id)sender;
@property NSDictionary *json;

@end


//このクラスでSessionManagerを使って天気を出すと、配列の天気が現在位置で更新されてしまい、お気に入りに現在地の天気が出るためここのみで記述している
@implementation PresentLocationView{
    NSMutableData *responseData;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self classFormatting];
    
    if(nil == self.locationManager) {
        self.locationManager = [[CLLocationManager alloc]init];
        
        if([[[UIDevice currentDevice] systemVersion]floatValue] >=8.0){
            
            [self.locationManager requestWhenInUseAuthorization];
        }
    }
    
    if(nil == self.locationManager)
        self.locationManager = [[CLLocationManager alloc]init];
    self.locationManager.delegate = self;
    
    [self.locationManager startUpdatingLocation];
    
    
    // Do any additional setup after loading the view, typically from a nib.
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)classFormatting{
    self.areaDataManager =[AreaDataManager ManageData];
    self.areaDataManager.delegate = self;
}


-(void)URLSession:(NSURLSession *)session
         dataTask:(NSURLSessionDataTask *)dataTask
didReceiveResponse:(NSURLResponse *)response
completionHandler:(void (^)(NSURLSessionResponseDisposition))completionHandler
{
    responseData = [NSMutableData data];
    
    
    completionHandler(NSURLSessionResponseAllow);
}

- (void)URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
    didReceiveData:(NSData *)data
{
    [responseData appendData:data];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray*)locations{
    CLLocation* location = [locations lastObject];
    
    
    double latitude = location.coordinate.latitude; //
    double longitude = location.coordinate.longitude; //
    
    
    NSString *urlString = [NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f&cnt=1&appid=216d8cade8f1f3ea1eb8d03d017f660d",latitude,longitude];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
    
    NSURLSessionDataTask *task = [session dataTaskWithURL:url];
    
    [task resume];
    
    
}

- (IBAction)weatherGetButton:(id)sender {
    if(responseData != nil ){
    self.json = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:nil];
    NSString *weather = [self.json[@"weather"] firstObject][@"main"];
    self.weatherLabel.text =[self.areaDataManager.getWeatherData valueForKeyPath:weather];
    }else{
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"位置情報が存在しません" message:@"位置情報をONにしてください" preferredStyle:UIAlertControllerStyleAlert];
                [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        }]];
        
        [self presentViewController:alertController animated:YES completion:nil];    }
}
@end
