//
//  SecondViewController.m
//  SearchBar
//
//  Created by SI3 on 2018/05/30.
//  Copyright © 2018年 D.I.System co.,ltd. All rights reserved.
//

#import "SecondViewController.h"
#import "SessionManager.h"
#import "ViewController.h"
#import "AreaDataManager.h"
#import "SVProgressHUD.h"

@interface SecondViewController ()<SessionManagerDelegate,AreaDataManagerDelegate>
@property (strong, nonatomic) SessionManager *sessionmanager;
@property (strong, nonatomic) AreaDataManager *areaDataManager;
@property (weak, nonatomic) IBOutlet UILabel *areaNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconImg;
@property (weak, nonatomic) IBOutlet UILabel *areaWeatherLabel;
@property (weak, nonatomic) IBOutlet UILabel *areaTempLabel;
@property (weak, nonatomic) IBOutlet UILabel *stringLabel;

@property (strong,nonatomic) NSArray *icon;


@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [SVProgressHUD show];
    
    [self singletonFormatter];
    [self.sessionmanager getWeatherData:[NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/weather?q=%@,jp&appid=216d8cade8f1f3ea1eb8d03d017f660d",self.myArea]];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)sendJsonData:(NSDictionary *)json {
    
    
    dispatch_async(
                   dispatch_get_main_queue(),
                   ^{
                       
                       NSDictionary *japan;
                       japan = self.areaDataManager.getDictionaryData;
                       
                       NSDictionary *japanWeather;
                       japanWeather =self.areaDataManager.getWeatherData;
                       
                       self.icon = [json[@"weather"] firstObject][@"icon"];
                       NSLog(@"%@", self.icon);
                       
                       NSString *setURL = [NSString stringWithFormat:@"http://openweathermap.org/img/w/%@.png",self.icon]; //icon処理
                       NSURL *imgURL = [NSURL URLWithString:setURL];
                       NSData * imgData = [NSData dataWithContentsOfURL:imgURL];
                       UIImage *weatherImage = [UIImage imageWithData:imgData];
                       self.iconImg.image = weatherImage;
                       
                       
                       NSString *areaName = [json valueForKeyPath:@"name"];
                       NSString *areaWeather = [json[@"weather"] firstObject][@"main"];
                       NSString *areaTemp =  [json valueForKeyPath:@"main.temp"];
                       self.areaNameLabel.text = [japan valueForKeyPath:areaName];
                       
                       
                       self.areaWeatherLabel.text =[japanWeather valueForKeyPath:areaWeather];
                       NSLog(@"%@", areaWeather);
                       NSLog(@"%@", areaName);
                       NSInteger degree  = [areaTemp intValue] - 273; //temp型から273度引いてdegree型に変換
                       self.areaTempLabel.text = [NSString stringWithFormat:@"%ld", (long)degree];
                       self.stringLabel.text =@"℃";
                       [SVProgressHUD dismiss];
                       
                   }
                   );
    
}

-(void)singletonFormatter{
    self.sessionmanager = [SessionManager sharedManager];
    self.sessionmanager.delegate = self;
    self.areaDataManager = [AreaDataManager ManageData];
    self.areaDataManager.delegate = self;
}

@end
