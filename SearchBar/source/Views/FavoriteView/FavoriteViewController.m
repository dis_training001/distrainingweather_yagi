//
//  FavoriteViewController.m
//  SearchBar
//
//  Created by SI3 on 2018/06/05.
//  Copyright © 2018年 D.I.System co.,ltd. All rights reserved.
//

#import "FavoriteViewController.h"
#import "SecondViewController.h"
#import "ViewController.h"
#import "TableViewCell.h"
#import "FavSessionManager.h"
#import "AreaDataManager.h"
#import "SVProgressHUD.h"

@interface FavoriteViewController ()<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate,FavSessionManagerDelegate,AreaDataManagerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (strong, nonatomic) FavSessionManager *FavSessionManager;
@property (strong, nonatomic) AreaDataManager *areaDataManager;
@property (strong, nonatomic) NSMutableArray *areaWeatherArray;
@property (strong, nonatomic) NSMutableArray *areaNameArray;
@property (strong, nonatomic) NSMutableArray *areaIcons;
@property (nonatomic, strong) NSArray *dataSourceSearchResult;
@property (weak,nonatomic)NSString *stringKey;
@property (nonatomic, strong) NSDictionary *favDic; //NSUserDefaultsを入れる配列
@property (nonatomic, strong) NSArray *favArray;
@property (nonatomic, strong) NSArray *searchAreaArray;
@property (nonatomic, strong) NSMutableArray *favMutableArray;
@property (nonatomic, strong) NSMutableArray *searchMutableArray;//NSUserDefaultsに上書きするための配列
@end

@implementation FavoriteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.favDic = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
    [self arrayFormatting];
    

}

-(void)viewDidAppear:(BOOL)animated{
    [SVProgressHUD show];
    [self classFormatting];
    [self arrayFormatting];
    self.favDic = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
    self.favArray = [self.favDic valueForKey:@"favorite"];
    self.searchAreaArray = [self.favDic valueForKey:@"areaSearch"];
    [self getWeatherDate];
    [self prepareTableView];
    [self.tableView reloadData];
    [SVProgressHUD dismiss];
}


- (void)sendFavJsonData:(NSDictionary *)favJson {
    dispatch_async(
                   dispatch_get_main_queue(),
                   ^{
                       NSArray *areaJsonWeather = [favJson valueForKeyPath:@"list.weather.main"];
                       NSArray *areaJsonName = [favJson valueForKeyPath:@"list.name"];
                       NSArray *areaJsonIcon = [favJson valueForKeyPath:@"list.weather.icon"];
                       
                       for (int i = 0; i < areaJsonWeather.count; i++) {
                           [self.areaNameArray addObject:areaJsonName[i]];
                           [self.areaWeatherArray addObject:areaJsonWeather[i][0]];
                           [self.areaIcons addObject:areaJsonIcon[i][0]];
                       }
                       

                       [self.tableView reloadData];
                   }
                   );
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void)classFormatting{
    self.FavSessionManager = [FavSessionManager sharedManager];
    self.FavSessionManager.delegate = self;
    self.areaDataManager =[AreaDataManager ManageData];
    self.areaDataManager.delegate = self;
    self.searchBar.delegate = self;
}


- (void)getWeatherDate{
    NSMutableString* favDataStr = [NSMutableString    string];
    for (NSInteger i = 0; i < self.favArray.count; i++) {
        if (i == 0) {
            [favDataStr setString:[self.areaDataManager.getAreaNumber valueForKeyPath:[NSString stringWithFormat:@"%@",self.favArray[i]]]];
            
        } else {
            [favDataStr appendString:@","];
            [favDataStr appendString:[self.areaDataManager.getAreaNumber valueForKeyPath:[NSString stringWithFormat:@"%@",self.favArray[i]]]];
        }
    }
    NSString *urlString = [NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/group?id=%@&appid=216d8cade8f1f3ea1eb8d03d017f660d",favDataStr];

    [self.FavSessionManager getWeatherData:urlString];
    
}


- (void)prepareTableView
{
    //デリゲート設定
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    // セルの設定
    NSString *cellName = NSStringFromClass([TableViewCell class]);
    [self.tableView registerNib:[UINib nibWithNibName:cellName bundle:nil] forCellReuseIdentifier:cellName];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ((self.dataSourceSearchResult).count == 0){
        //検索用配列に一件もなければ通る
        return (self.areaNameArray).count;
    }else{
        return (self.dataSourceSearchResult).count;
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellName = NSStringFromClass([TableViewCell class]);
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName forIndexPath:indexPath];
    
    [cell.favButton addTarget:self action:@selector(handleTouchButton:event:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if ((self.dataSourceSearchResult).count == 0){
        cell.searchAreaLabel.text =nil;
        UIImage *img = [UIImage imageNamed:@"fav"];
        [cell.favButton setBackgroundImage:img forState:UIControlStateNormal];
        cell.weatherLabel.text = [self.areaDataManager.getWeatherData valueForKeyPath:self.areaWeatherArray[indexPath.row]];
        cell.areaLabel.text = [self.areaDataManager.getDictionaryData valueForKeyPath:self.areaNameArray[indexPath.row]];
        cell.iconImg.image =  [UIImage imageNamed:self.areaIcons[(long)indexPath.row]];
        cell.favButton.hidden = NO;

    }else{
        //検索用配列に値が入っていた場合こっちの処理へ
        cell.searchAreaLabel.text = [self.areaDataManager.getDictionaryData valueForKeyPath:self.dataSourceSearchResult[(long)indexPath.row]];
        cell.areaLabel.text = nil;
        cell.weatherLabel.text = nil;
        cell.iconImg.image = nil;
        cell.favButton.hidden = YES;
    }
    
    
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ((self.dataSourceSearchResult).count == 0){
        self.stringKey = self.areaNameArray[(long)indexPath.row];
    }else{
        self.stringKey = self.dataSourceSearchResult[(long)indexPath.row];
    }
    [self performSegueWithIdentifier:@"GO" sender:self];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"GO"]) {
        SecondViewController *vc = segue.destinationViewController;
        vc.myArea = self.stringKey ;
    }
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if([searchBar.text length] == 0) {
        [self.tableView reloadData];
    }
    [self search:searchBar.text];
}



- (void)search:(NSString *)search {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains %@", search];
    
    NSArray *searchArrays = [NSArray array];
    NSMutableArray *searchMutableArray = [NSMutableArray array];
    

    searchArrays = self.searchMutableArray;
    
    searchArrays = [searchArrays filteredArrayUsingPredicate:predicate];
    
    for (NSString* searchStr in searchArrays) {
        NSArray* data = [self.areaDataManager.getDictionaryKeyData valueForKeyPath:searchStr];
        if (data != nil) {
            [searchMutableArray addObject:data];
        }
    }
    //検索結果を配列にしたものを上書き
    self.dataSourceSearchResult = searchMutableArray;
    [self.tableView reloadData];
}


- (void)handleTouchButton:(UIButton *)sender event:(UIEvent *)event {
    //ボタンを押した場所にある地名をfavoriteが鍵の配列に格納
    NSIndexPath *indexPath = [self indexPathForControlEvent:event];
    NSString *test = [self.areaDataManager.getDictionaryData valueForKey:self.areaNameArray[indexPath.row]];
    NSString* delArea = nil;
    self.searchMutableArray = [[self.favDic valueForKey:@"areaSearch"] mutableCopy];
    
    
    for(NSString* area in self.searchMutableArray){
        if([area isEqualToString:test]){
            delArea = test;
            break;
        }
    }
    if (delArea != nil) {
        // 重複する県があるならお気に入りから削除
        [self.searchMutableArray removeObject:delArea];
        NSLog(@"%@", _searchMutableArray);
    }
    [[NSUserDefaults standardUserDefaults] setObject:self.searchMutableArray forKey:@"areaSearch"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.favMutableArray = [[self.favDic valueForKey:@"favorite"] mutableCopy];
    self.favDic = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
    
    NSString* delFav = nil;
    for(NSString* test in self.favMutableArray){
        if([test isEqualToString:self.areaNameArray[indexPath.row]]){
            delFav = test;
            break;
        }
    }
    
    if (delFav != nil) {
        // 重複する県があるならお気に入りから削除
        [self.favMutableArray removeObject:delFav];
        [self.favMutableArray removeObject:_areaNameArray];
    } 
    
    [[NSUserDefaults standardUserDefaults] setObject:self.favMutableArray forKey:@"favorite"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.favDic = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
    self.favArray = [self.favDic valueForKey:@"favorite"];
    [self classFormatting];
    [self arrayFormatting];
    [self getWeatherDate];
    [self prepareTableView];
    [self.tableView reloadData];
}

- (NSIndexPath *)indexPathForControlEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint p = [touch locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
    return indexPath;
}


- (void)arrayFormatting{
    self.areaWeatherArray= [NSMutableArray array];
    self.areaNameArray= [NSMutableArray array];
    self.areaIcons= [NSMutableArray array];
    self.searchAreaArray = [NSArray array];
    self.searchMutableArray = [NSMutableArray array];
    self.searchMutableArray = [[self.favDic valueForKey:@"areaSearch"] mutableCopy];
    NSLog(@"%@", _searchMutableArray);
}

@end
