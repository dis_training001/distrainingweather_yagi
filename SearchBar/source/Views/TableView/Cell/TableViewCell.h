//
//  TableViewCell.h
//  SearchBar
//
//  Created by SI3 on 2018/05/28.
//  Copyright © 2018年 D.I.System co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *searchAreaLabel;
@property (weak, nonatomic) IBOutlet UILabel *areaLabel;
@property (weak, nonatomic) IBOutlet UILabel *weatherLabel;
@property (weak, nonatomic) IBOutlet UIButton *favButton;
@property (weak, nonatomic) IBOutlet UIImageView *iconImg;
@property (nonatomic) NSDictionary *info;
@end
