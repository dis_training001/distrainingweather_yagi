//
//  ViewController.m
//  SearchBar
//
//  Created by SI3 on 2018/05/28.
//  Copyright © 2018年 D.I.System co.,ltd. All rights reserved.
//

#import "SecondViewController.h"
#import "ViewController.h"
#import "TableViewCell.h"
#import "SessionManager.h"
#import "AreaDataManager.h"
#import "SVProgressHUD.h"

@interface ViewController ()<UITableViewDelegate, UITableViewDataSource,UISearchDisplayDelegate, UISearchBarDelegate,SessionManagerDelegate,AreaDataManagerDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) SessionManager *sessionmanager;
@property (strong, nonatomic) AreaDataManager *areaDataManager;

@property (nonatomic) NSMutableArray *weatherArray; //３回にわけて取った天気だけを入れる配列
@property (nonatomic) NSMutableArray *mainAreaNameArray; //上の地名版
@property (nonatomic) NSMutableArray *IconArray; //上のアイコン版
@property (weak,nonatomic)NSString *stringKey; //次の画面に受け渡す用の変数
@property (nonatomic, strong) NSArray *dataSourceSearchResult; //曖昧検索で引っかかった地名を格納する配列
@property (nonatomic, strong) NSDictionary *favDic; //NSUserDefaultsを入れる配列
@property (nonatomic, strong) NSMutableArray *favMutableArray;//NSUserDefaultsに上書きするための配列
@property (nonatomic, strong) NSArray *favArray;//NSUserDefaultsの鍵favoriteを格納する配列
@property (nonatomic, strong) NSMutableArray *searchMutableArray;//NSUserDefaultsに上書きするための配列

@end

@implementation ViewController

- (void)viewDidLoad {
    [SVProgressHUD show];
    [super viewDidLoad];
    [self startUp];
    [SVProgressHUD dismiss];
}


-(void)viewDidAppear:(BOOL)animated{
    [SVProgressHUD show];
    [self appearSet];
    [SVProgressHUD dismiss];
    
}


- (void)sendJsonData:(NSDictionary *)json {
    dispatch_async(
                   dispatch_get_main_queue(),
                   ^{
                       
                       NSArray *areaWeather = [json valueForKeyPath:@"list.weather.main"];
                       NSArray *areaName = [json valueForKeyPath:@"list.name"];
                       NSArray *areaIcon = [json valueForKeyPath:@"list.weather.icon"];
                       
                       for (int i = 0; i < areaWeather.count; i++) {
                           [self.mainAreaNameArray addObject:areaName[i]];
                           [self.weatherArray addObject:areaWeather[i][0]];
                           [self.IconArray addObject:areaIcon[i][0]];
                       }
                       [self.tableView reloadData];
                       NSLog(@"%lu",self.mainAreaNameArray.count);

                       if((!(self.mainAreaNameArray.count == 8))&&(!(self.mainAreaNameArray.count == 27))&&(!(self.mainAreaNameArray.count == 47))){
//                           [self appearSet];
                           
                       }
                       
                   }
                   );
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)arrayFormatting{
    self.weatherArray= [NSMutableArray array];
    self.mainAreaNameArray= [NSMutableArray array];
    self.IconArray= [NSMutableArray array];
    self.dataSourceSearchResult = [NSArray array];
    self.favMutableArray = [NSMutableArray array];
    self.searchMutableArray = [NSMutableArray array];
}


-(void)classFormatting{
    self.sessionmanager = [SessionManager sharedManager];
    self.sessionmanager.delegate = self;
    self.areaDataManager =[AreaDataManager ManageData];
    self.areaDataManager.delegate = self;
    self.searchBar.delegate = self;
}


- (void)getWeatherDate{
    for(NSString* urlString in self.areaDataManager.jsonAddressArray){
        [self.sessionmanager getWeatherData:urlString];
        [NSThread sleepForTimeInterval:0.1];
       }

}




- (void)prepareTableView
{
    //デリゲート設定
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    // セルの設定
    NSString *cellName = NSStringFromClass([TableViewCell class]);
    [self.tableView registerNib:[UINib nibWithNibName:cellName bundle:nil] forCellReuseIdentifier:cellName];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ((self.dataSourceSearchResult).count == 0){
        //検索用配列に一件もなければ通る
        return (self.mainAreaNameArray).count;
    }else{
        return (self.dataSourceSearchResult).count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellName = NSStringFromClass([TableViewCell class]);
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName forIndexPath:indexPath];
    
    [cell.favButton addTarget:self action:@selector(handleTouchButton:event:) forControlEvents:UIControlEventTouchUpInside];
    
    if ((self.dataSourceSearchResult).count == 0){//検索してない時の処理
        cell.weatherLabel.text =[self.areaDataManager.getWeatherData valueForKeyPath:self.weatherArray[(long)indexPath.row]];
        cell.areaLabel.text = [self.areaDataManager.getDictionaryData valueForKeyPath:self.mainAreaNameArray[(long)indexPath.row]];
        cell.favButton.hidden = NO;
        cell.searchAreaLabel.text = nil;
        //areaLabelのテキストがお気に入りと一致した場合星をつける
        NSArray* favoriteArray = [self.favDic valueForKey:@"favorite"];
        NSString* equalFavString = [self.areaDataManager.getDictionaryKeyData valueForKeyPath:cell.areaLabel.text];
        
        //☆の画像を貼り付け
        UIImage *unfav = [UIImage imageNamed:@"unfav"];
        [cell.favButton setBackgroundImage:unfav forState:UIControlStateNormal];
        
        //[cell.favButton setTitle:@"☆" forState:UIControlStateNormal];
        for (NSString* fav in favoriteArray) {
            if ([equalFavString isEqualToString:fav]) {
                
                //お気に入りとラベルの文字が一致で★の画像を貼り付け
                UIImage *favimg = [UIImage imageNamed:@"fav"];
                [cell.favButton setBackgroundImage:favimg forState:UIControlStateNormal];
                break;
            }
        }

        cell.iconImg.image =  [UIImage imageNamed:self.IconArray[(long)indexPath.row]];
    }else{
        //検索用配列に値が入っていた場合こっちの処理へ
        cell.searchAreaLabel.text = [self.areaDataManager.getDictionaryData valueForKeyPath:self.dataSourceSearchResult[(long)indexPath.row]];
        cell.areaLabel.text = nil;
        cell.weatherLabel.text =[self.areaDataManager.getWeatherData valueForKeyPath:self.dataSourceSearchResult[(long)indexPath.row]];
        cell.iconImg.image = nil;
        cell.favButton.hidden = YES;
    }
    return cell;
}




- (void)handleTouchButton:(UIButton *)sender event:(UIEvent *)event {
    //ボタンを押した場所にある地名をfavoriteが鍵の配列に格納
    NSIndexPath *indexPath = [self indexPathForControlEvent:event];
    NSString *test = [self.areaDataManager.getDictionaryData valueForKey:self.mainAreaNameArray[indexPath.row]];
    NSString* delArea = nil;
    self.searchMutableArray = [[self.favDic valueForKey:@"areaSearch"] mutableCopy];

    
    for(NSString* area in self.searchMutableArray){
        if([area isEqualToString:test]){
            delArea = test;
            break;
        }
    }

    
    if (delArea != nil) {
        // 重複する県があるならお気に入りから削除
        [self.searchMutableArray removeObject:delArea];
    }else{
        // お気に入りにないときは追加
        [self.searchMutableArray addObject:test];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:self.searchMutableArray forKey:@"areaSearch"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    

    self.favMutableArray = [[self.favDic valueForKey:@"favorite"] mutableCopy];
    self.favDic = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
    
    
    NSString* delFav = nil;
    for(NSString* test in self.favMutableArray){
        if([test isEqualToString:self.mainAreaNameArray[indexPath.row]]){
            delFav = test;
            break;
        }
    }
    
    if (delFav != nil) {
        // 重複する県があるならお気に入りから削除
        [self.favMutableArray removeObject:delFav];
    } else {
        // お気に入りにないときは追加
        [self.favMutableArray addObject:self.mainAreaNameArray[indexPath.row]];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:self.favMutableArray forKey:@"favorite"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    
    

    
    
    self.favDic = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
    [self.tableView reloadData];
}


- (NSIndexPath *)indexPathForControlEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint p = [touch locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
    return indexPath;
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"GO"]) {
        SecondViewController *vc = segue.destinationViewController;
        vc.myArea = self.stringKey ;
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ((self.dataSourceSearchResult).count == 0){
        //検索用配列に格納されていない場合通常の配列から値を受け渡し
        self.stringKey = self.mainAreaNameArray[(long)indexPath.row];
    }else{
        //検索用配列に値がある場合検索用配列の順番から受けわたす
        self.stringKey = self.dataSourceSearchResult[(long)indexPath.row];
    }
    [self performSegueWithIdentifier:@"GO" sender:self];
}


- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText{
    
    if([searchBar.text length] == 0) {
        [self.tableView reloadData];
    }
    [self search:searchBar.text];
}


- (void)search:(NSString *)search {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains %@", search];
    
    NSArray *searchArrays = [NSArray array];
    NSMutableArray *searchMutableArray = [NSMutableArray array];
    
    searchArrays = self.areaDataManager.getAreaData;
    searchArrays = [searchArrays filteredArrayUsingPredicate:predicate];
    
    for (NSString* searchStr in searchArrays) {
        NSArray* data = [self.areaDataManager.getDictionaryKeyData valueForKeyPath:searchStr];
        if (data != nil) {
            [searchMutableArray addObject:data];
        }
    }
    //検索結果を配列にしたものを上書き
    self.dataSourceSearchResult = searchMutableArray;
    [self.tableView reloadData];
}


- (IBAction)hidden:(id)sender {
    NSArray* nilarray =[NSArray array];
    [[NSUserDefaults standardUserDefaults] setObject:nilarray forKey:@"areaSearch"];
    [[NSUserDefaults standardUserDefaults] setObject:nilarray forKey:@"favorite"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.tableView reloadData];
    [self prepareTableView];
}


-(void)startUp{
    [self classFormatting];
    [SVProgressHUD dismiss];
}

- (void)appearSet{
    [self arrayFormatting];
    self.favDic = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
    self.favArray = [self.favDic valueForKey:@"favorite"];
    [self getWeatherDate];
    [self prepareTableView];
    [self.tableView reloadData];
}

@end

