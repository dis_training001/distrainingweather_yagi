//
//  main.m
//  SearchBar
//
//  Created by SI3 on 2018/05/28.
//  Copyright © 2018年 D.I.System co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
